# Adblock Plus Options Page: Style Guide

This this the style guide for Adblock Plus desktop and mobile browser extension.

## Index

1. [Typography](#typography)
1. [Colour](#colour)
1. [Navigation](#navigation)
1. [Section style](#section-style)
1. [Checkbox](#checkbox)
1. [Radio buttons](#radio-buttons)
1. [Buttons](#buttons)
1. [Popups](#popups)
1. [Dropdown](#dropdown)
1. [Modal overlays](#modal-overlays)
1. [Tables](#tables)
1. [Text box](#text-box)

## Typography

### Desktop

Font: Source Sans Pro

Line height: 1.3em

# H1: Main Heading - 300 (light), 3em, #4A4A4A

## H2: Section header - 700 (bold), 1.125em, #4A4A4A

### H3: Bold content within body or form labels - 700 (bold), 1em, #4A4A4A / #FFF

Body: Regular, 400 (regular), 1em, #4A4A4A

**Body emphasized: 700 (bold), 1em #4A4A4A**

Text link: 400 (regular), 1em #077CA6 (underlined)

**LABEL: BUTTON TEXT - 700 (bold), 1em, #077CA6 / #FFF**

### Mobile

# H1: Main Heading - 400 (regular), 1.15em, #4A4A4A

## H2: Section header - 700 (bold), 1.1em, #4A4A4A

Body: 400 (regular), 1em #4A4A4A

**Body emphasized: 700 (bold), 1em #4A4A4A**

Text link: 400 (regular), 1em #077CA6

**LABEL: BUTTON TEXT - 700 (bold), 1em, #077CA6 / #FFF**

Line height: 2em

## Color

![](/res/abp/settings-style-guide/color-light-grey.jpg)
![](/res/abp/settings-style-guide/color-grey.jpg)
![](/res/abp/settings-style-guide/color-dark-grey.jpg)
![](/res/abp/settings-style-guide/color-black.jpg)
![](/res/abp/settings-style-guide/color-clear-blue.jpg)
![](/res/abp/settings-style-guide/color-light-blue.jpg)
![](/res/abp/settings-style-guide/color-blue.jpg)
![](/res/abp/settings-style-guide/color-green.jpg)
![](/res/abp/settings-style-guide/color-red.jpg)
![](/res/abp/settings-style-guide/color-light-yellow.jpg)
![](/res/abp/settings-style-guide/color-light-orange.jpg)
![](/res/abp/settings-style-guide/color-premium-gold.png)
![](/res/abp/settings-style-guide/color-dark-premium-gold.png)

## Navigation

![](/res/abp/settings-style-guide/navigation-menu.jpg)

For wider screens increase the width of the section area (second column) and center the entire options page.

## Section style 

![](/res/abp/settings-style-guide/section-style.jpg)

## Checkbox 

| State | Image |
|------|------|
| Rest state | ![](/res/abp/settings-style-guide/checkbox-1-rest-state.jpg) |
| Hover-over state | ![](/res/abp/settings-style-guide/checkbox-2-hover-over-state.jpg) |
| Click state | ![](/res/abp/settings-style-guide/checkbox-3-click-state.jpg) |
| Selected state | ![](/res/abp/settings-style-guide/checkbox-4-selected-state.jpg) |

## Radio buttons

![](/res/abp/settings-style-guide/radio-buttons.jpg)

## Buttons

| State | Image |
|------|------|
| Primary button (active) | ![](/res/abp/settings-style-guide/primary-button-1-active.jpg) |
| Primary button (hover) | ![](/res/abp/settings-style-guide/primary-button-2-hover.jpg) |
| Primary button (inactive) | ![](/res/abp/settings-style-guide/primary-button-3-inactive.jpg) |
| Secondary button (active) | ![](/res/abp/settings-style-guide/secondary-button-1-default.jpg) |
| Secondary button (hover) | ![](/res/abp/settings-style-guide/secondary-button-2-hover.jpg) |
| Upgrade button (active) | ![](/res/abp/settings-style-guide/upgrade-button-active.png) |
| Upgrade button (hover) | ![](/res/abp/settings-style-guide/upgrade-button-hover.png) |
| Premium button (active) | ![](/res/abp/settings-style-guide/premium-button-active.png) |
| Premium button (hover) | ![](/res/abp/settings-style-guide/premium-button-hover.png) |

## Popups

### Tooltips

![](/res/abp/settings-style-guide/tooltip-style.jpg)

### Gear icon

![](/res/abp/settings-style-guide/gear-popup.jpg)

## Notifications

### Banner notifications

![](/res/abp/settings-style-guide/whitelisted-websites-notification.jpg)

### In-page notifications
![](/res/abp/settings-style-guide/in-page-notifications.jpg)

## Dropdown

| State | Image |
|------|------|
| Rest state (one language) | ![](/res/abp/settings-style-guide/language-1-rest.jpg) |
| Hover-over state | ![](/res/abp/settings-style-guide/language-2-hover.jpg) |
| Language overlay | [See Dropdown overlay](#dropdown-overlay) |
| New language added | ![](/res/abp/settings-style-guide/language-3-multiple.jpg) [1] |

[1] Border highlight disappears after there has been a new interaction on the page.

## Modal overlays

### Dropdown overlay

![](/res/abp/settings-style-guide/language-overlay.jpg)

#### Mobile dropdown

![](/res/abp/mobile-settings/style-guide/style-firefox-mob-options-page-filter-subscriptions.jpg)

 See also [Mobile style guide](#mobile-style-guide)
 
### Informational overlay

![](/res/abp/settings-style-guide/about-overlay.jpg)

### Form overlay

#### Form default

![](/res/abp/settings-style-guide/modal-window-form-default.jpg)

#### Form valid

![](/res/abp/settings-style-guide/modal-window-form-valid.jpg)

#### Form error

![](/res/abp/settings-style-guide/modal-window-form-error.jpg)

##### Mobile form overlay

![](/res/abp/mobile-settings/style-guide/style-firefox-mob-options-page-url-filter.jpg)

See also [Mobile style guide](#mobile-style-guide)

## Tables

### Default

![](/res/abp/settings-style-guide/table-style.jpg)

### Complex table

![](/res/abp/settings-style-guide/complex-table.jpg)

### Empty state

![](/res/abp/settings-style-guide/empty-state.jpg)

## Text box

### Default state

![](/res/abp/settings-style-guide/text-box.jpg)

### Active state

![](/res/abp/settings-style-guide/text-box-hover.jpg)

### Error state

![](/res/abp/settings-style-guide/text-box-error.jpg)

## Mobile style guide

### Mobile home screen

![](/res/abp/mobile-settings/style-guide/style-firefox-mob-options-page.jpg)

### Mobile modal screen

See [Mobile form overlay](#mobile-form-overlay)

### Mobile modal dropdown

See [Mobile dropdown](#mobile-dropdown)
